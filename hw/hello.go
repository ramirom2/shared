package hw

import "fmt"

func SayHello() {
	fmt.Println("Hello")
}

func SayHelloWithName(name string) {
	fmt.Println("Hello " + name)
}
